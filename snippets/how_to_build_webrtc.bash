#!/bin/bash
cd ~
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=~/depot_tools:$PATH
mkdir webrtc
cd webrtc
fetch --nohooks webrtc_android
#git checkout thebranchyouwant (or don't)
gclient sync
cd src
./tools_webrtc/android/build_aar.py
