#!/bin/sh

set -e
BACKUP_DIR=/home/lockywolf/DevLinux/linuxbugs-lwf-for-generalized-bugfixes-and-small-scripts/scripts/

/bin/cp /root/.bashrc $BACKUP_DIR/root_bashrc.sh
/bin/cp /etc/profile.d/z-lwf_common_aliases.sh $BACKUP_DIR/profiled_z-commonaliases.sh
/bin/cp /etc/profile.d/z-lwf_ps1+ps2.sh $BACKUP_DIR/profiled_z-ps1+ps2.sh
/bin/cp /etc/profile.d/z-lwf_less.sh $BACKUP_DIR/profiled_z-less.sh
/bin/cp /etc/profile.d/z-lwf_jdk.sh $BACKUP_DIR/profiled_z-java_gtk.sh

/bin/cp /etc/profile.d/z-lwf_userdir-bin-path.sh $BACKUP_DIR/profiled_z-lwf_userdir-bin-path.sh
/bin/cp -r /etc/cron.hourly $BACKUP_DIR/etc_cron.hourly
/bin/cp -r /etc/issue.d $BACKUP_DIR/etc_issue.d


pushd /home/lockywolf/BACKUP/00-Etc-Git-All-Machines/
pushd server.lockywolf.net/etc
git pull
popd
pushd delllaptop.lockywolf.net/etc
git pull


dt=$(date -Idate)
l_bd=/home/lockywolf/BACKUP/01-FirefoxSessions/$dt/
mkdir -p $l_bd
#cp -v --no-clobber /home/lockywolf/.mozilla/firefox/*.default/sessionstore-backups/previous.jsonlz4 $l_bd
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'previous.jsonlz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'recovery.jsonlz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'recovery.baklz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done


# find /home/lockywolf/BACKUP/01-FirefoxSessions/

