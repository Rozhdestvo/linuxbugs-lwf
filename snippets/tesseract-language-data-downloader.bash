#!/bin/bash

declare LANGS

LANGS=(grc  epo_alt  eng  ukr  tur  tha  tgl  tel  tam  swe  swa  srp  sqi  spa_old  spa  slv  slk  ron  por  pol  nor  nld  msa  mlt  mkd  mal  lit  lav  kor  kan  ita_old  ita  isl  ind  chr  hun  hrv  hin  heb  glg  frm  frk  fra  fin  eus  est  equ  epo  enm  ell  due  dan  ces  cat  bul  ben  bel  aze  ara  afr  jpn  chi_sim  chi_tra  rus  vie)

for var in "${LANGS[@]}"
do
    aria2c -x120 --min-split-size=148576 --split=120 --auto-file-renaming=false "https://raw.githubusercontent.com/tesseract-ocr/tessdata/4.0.0/$var.traineddata"
done

	   
