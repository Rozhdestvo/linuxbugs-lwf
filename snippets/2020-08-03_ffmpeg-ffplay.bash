#!/bin/bash
# the -input_format mjpeg is the trick that makes the webcam produce compressed video stream
# otherwise it cannot make 30 frames per second
ffplay -f video4linux2 -input_format mjpeg -framerate 30 -video_size 640x480 /dev/video0
