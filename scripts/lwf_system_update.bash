#!/bin/bash

# Doing updates
slackpkg update
slackpkg install-new
slackpkg upgrade-all

# Backing up root
time rsync -v --archive --hard-links --acls --xattrs --inplace --one-file-system --delete-before --fuzzy --human-readable --info=progress2 --partial / /mnt/hd/ --exclude='/tmp/*'
