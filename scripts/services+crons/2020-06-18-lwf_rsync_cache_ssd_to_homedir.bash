#!/bin/bash -x

set -uoe pipefail


declare USERNAME
USERNAME="$1"
if getent passwd "$USERNAME"
then
    printf "User %s exists, going on\n" "$USERNAME"
else
    printf "incorrect user\n"
    exit 1
fi

if [[ "$(whoami)" = "$USERNAME" ]]
then
    printf "correct user %s\n" "$USERNAME"
else
    printf "incorrect user\n"
    exit 1
fi

declare LOCKFILE
LOCKFILE="/var/lock/rsync-$USERNAME-cache-40e6219c-aefd-11ea-814f-5c260a7108b9"
if [[ -e "$LOCKFILE" ]]
then
    printf "Lockfile exists, previous process not finished? (%s)\n" "$LOCKFILE"
    zenity --error --text "Lockfile exists ($LOCKFILE). Previous process running?"
    exit 1
fi

touch "$LOCKFILE"


declare CACHE_ROOT
CACHE_ROOT="/mnt/manual_cache"


if findmnt "$CACHE_ROOT"
then
    printf "Cache seems to be mounted: %s\n" "$CACHE_ROOT"
else
    printf "Cache seems to be not mounted: %s : error\n" "$CACHE_ROOT"
fi

declare -a DIRS_TO_SYNC
DIRS_TO_SYNC=( ".recoll" ".cache" )


for dirname_idx in "${DIRS_TO_SYNC[@]}"
do
    declare SYNCDIR
    SYNCDIR="/home/$USERNAME/$dirname_idx"
    if [[ -e "$SYNCDIR" ]]
    then
	printf "want to sync %s, and it exists\n" "$SYNCDIR"
	#note the slash (/) at the end of SYNCDIR
	if time ionice -c 3 rsync -aHAXLhxu --delete-after "$SYNCDIR/" "$SYNCDIR.sync"
	then
	    printf "Seems to have synced %s ok\n" "$SYNCDIR"
	fi
    else
	printf "%s does not seem to exist, cannot sync\n" "$SYNCDIR"
	zenity --display=:0.0 --error --text="rsync-cache $SYNCDIR does not exist"
    fi
done

rm "$LOCKFILE"
date
date > /var/tmp/lwf_rsync_cache_ssd_to_homedir.bash.last-run
printf "sync finished \n"

