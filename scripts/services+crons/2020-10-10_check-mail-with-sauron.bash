#!/bin/bash
# the mu binary
MU=mu
# put the path to your Inbox folder here
CHECKDIR="/home/$LOGNAME/Mail-mbsync/"

sauron_msg () {
    DBUS_COOKIE="/home/$LOGNAME/.sauron-dbus"
    if test "x$DBUS_SESSION_BUS_ADDRESS" = "x"; then
	if test -e $DBUS_COOKIE; then
	    export DBUS_SESSION_BUS_ADDRESS="`cat $DBUS_COOKIE`"
	fi
    fi
    if test -n "x$DBUS_SESSION_BUS_ADDRESS"; then
	# printf "debug:Printing command: %s\n" "dbus-send --session	\
	# 	  --dest=\"org.gnu.Emacs\" \
	# 	  --type=method_call \
	# 	    \"/org/gnu/Emacs/Sauron\" \
	# 	    \"org.gnu.Emacs.Sauron.AddMsgEvent\" \
	# 	    string:shell uint32:3 string:\"$1\""
	dbus-send --session	\
		  --dest="org.gnu.Emacs" \
		  --type=method_call \
		    "/org/gnu/Emacs/Sauron" \
		    "org.gnu.Emacs.Sauron.AddMsgEvent" \
		    string:shell uint32:3 string:"$1" || exit 1
    fi
}
#
# -mmin -5: consider only messages that were created / changed in the
# the last 5 minutes
#
# for f in `find $CHECKDIR -mmin -5 -a -type f -not -iname '.uidvalidity'`; do
#     subject=$($MU view "$f" | grep '^Subject:' | sed 's/^Subject://')
#     sauron_msg "mail: $subject"
# done


readarray -d '' arrayName < <(find "$CHECKDIR" -mmin -6 -a -type f -a -not -iname '.uidvalidity' -print0)
for f in "${arrayName[@]}"
do
    #    printf "debug:%s\n" "$f"
    subject=$($MU view "$f" | grep '^Subject:' | sed 's/^Subject://')
    sauron_msg "mail: $subject"
done
