#!/bin/bash

readarray arrayFirefox < <(wmctrl -lxp | grep -i Navigator\.Firefox | gawk '{print $1}')
readarray arrayEmacs < <(wmctrl -lxp | grep -i emacs\.Emacs | gawk '{print $1}')
readarray arrayPidgin < <(wmctrl -lxp | grep -i Pidgin\.Pidgin | gawk '{print $1}')


wmctrl -r 'hexchat' -t 9

wmctrl -r 'discord' -t 4

for f in "${arrayFirefox[@]}"
do
    wmctrl -i -r "$f" -t 3
done

for f in "${arrayEmacs[@]}"
do
    wmctrl -i -r "$f" -t 2
done

for f in "${arrayPidgin[@]}"
do
    wmctrl -i -r "$f" -t 4
done


wmctrl -r 'xfce4-terminal' -t 1

wmctrl -x -r 'skype.skype' -t 4
wmctrl -x -r 'slack.slack' -t 4
wmctrl -x -r 'zoom.zoom' -t 4


