#!/usr/bin/bash

case "$1" in
    up)
	printf "case up\n"
	nmcli connection up vultr-seoul-openbsd-wireguard-simple
	;;
    down)
	printf "case down\n"
	nmcli connection down vultr-seoul-openbsd-wireguard-simple
	;;
    status)
	printf "not implemented\n"
	;;
    *) printf "usage: {up,down,status}\n"
esac

#nmcli connection up vultr-seoul-openbsd-wireguard-simple
