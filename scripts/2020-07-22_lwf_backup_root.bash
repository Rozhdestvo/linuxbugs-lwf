#!/bin/bash
time ionice -c 3 rsync -v --archive --hard-links --acls --xattrs --inplace --one-file-system --delete-before --fuzzy --human-readable --info=progress2 --partial / /mnt/root_backup/ --exclude='/tmp/*' --exclude='/etc/fstab'
