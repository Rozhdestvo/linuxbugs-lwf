#!/bin/sh

#if [[ -eq "$(whoami)" "root" ]] ; then
#  print 'I am root'
#fi

declare KERNEL_VERSION
KERNEL_VERSION="$(find /boot -iname 'vmlinuz-huge*' -type f | sort -r | sed 1q | cut -c 20-)"

slackpkg -batch=on reinstall xorg mesa libglvnd
removepkg nvidia-legacy390-driver
sbopkg -e continue -B -i nvidia-legacy390-driver:COMPAT32=yes:CURRENT=yes -i nvidia-legacy390-kernel:KERNEL="$KERNEL_VERSION" <<END
c
END


