#!/bin/sh
# This file is expected to be placed in /etc/profile.d/less.sh
# Colour support for 'less'
 
export LESS="-R $LESS"
