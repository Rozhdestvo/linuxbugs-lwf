#!/bin/bash

dt=$(date -Iseconds)
l_bd=/home/lockywolf/BACKUP/01-FirefoxSessions/$dt/
mkdir -p $l_bd
#cp -v --no-clobber /home/lockywolf/.mozilla/firefox/*.default/sessionstore-backups/previous.jsonlz4 $l_bd
suffix=1
for fname in $(find /home/lockywolf/.mozilla/firefox/ -name 'previous.jsonlz4' -print)
do
    bn=$(basename $fname)
    cp -n $fname $l_bd/$bn.$suffix
    suffix=$(( suffix + 1 ))
done

