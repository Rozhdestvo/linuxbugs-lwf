#!/bin/bash

declare -i I
I=1
while true;
do
  printf "Attempt %d, time %s.\n" "$I" "$(/sbin/clock)"
  I=$((I+1))
  time mbsync -aV
  sleep 30
done
  