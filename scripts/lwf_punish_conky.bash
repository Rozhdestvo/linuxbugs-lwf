#!/bin/bash
set -uoe pipefail

if pgrep conky
then 
  true
else 
  DISPLAY=:0 zenity --error --text "Conky not running" &
fi