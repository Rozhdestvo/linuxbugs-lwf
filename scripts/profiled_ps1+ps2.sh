#!/bin/bash
# This file is expected to be placed in /etc/profile.d/ps1+ps2.sh

## Set the main prompt.
# TAG:BUG This code doesn't support ksh.
# blue colour  smth 
#export PS1="\$?"
#if [ \$? = 0 ] ; then \e[1;31m\$?\e[m ; else \e[1;32m\$?\e[m ; fi ;
# export PS1="$(if [ \$? = 0 ] ; then echo \"\e[1;31m\$?\e[m\" ; else echo \"\e[1;32m\$?\e[m\" ; fi ;) :"
#export PS1="$(if [ \$? = 0 ] ; then echo equal ;\$? else echo not equal\$? ; fi ;) :"
# TODO: Add conditional colour. If retval is 0, white, otherwise, red.
export PROMPT_COMMAND='export LWF_RETVAL=$?;'
export PS1="\[\e]2;\$(hostname -s)%\$(whoami)@\$PWD(\$TERM)\a\]\D{%F-%a-%T}|history#\!|prompt#\#|jobs#\j|s\e[1;31m$(echo \$LWF_RETVAL)\e[m|/dev/pts/\l.\u@\H:\w\n\\$:"

## Set the long line prompt.
export PS2="continue-> "
## Set the debugging prompt.
export PS4='$0P:$LINENO:+ '

# Not used. TODO?
#export PROMPT_COMMAND="echo -n [$(date +%k:%m:%S)]"


# https://www.thegeekstuff.com/2008/09/bash-shell-ps1-10-examples-to-make-your-linux-prompt-like-angelina-jolie/
# https://wiki.archlinux.org/index.php/Bash/Prompt_customization
# https://www.gnu.org/software/bash/manual/bashref.html#GNU-Parallel
