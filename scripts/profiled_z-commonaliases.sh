#!/bin/sh
# This file is expected to be placed in /etc/profile.d/common_aliases.sh
#launch windowed emacs from the console
alias emw="emacsclient -n -a ''"
alias emc="emacsclient -nw -a ''"
#slackpkg
alias spku="slackpkg update"
alias spkua="slackpkg install-new && slackpkg upgrade-all"
#sbopkg
alias sbog="sbopkg -g"
alias sbos='sbopkg -s'
#git
alias gits="git status"
alias gitd="git --no-pager diff"
alias gitfix="git commit -a --amend --no-edit"
#no funny symbols
alias catt="cat -t"
#wget always continue
alias wget='wget -c --content-disposition --xattr'

# curl
alias curltouch='curl --head'
alias curlget='curl --styled-output -O -C - '

# 2020-07-24_Disabled-because-internet-is-slow
#alias sborefresh="cd /var/lib/sbopkg/ && rm -rf SBo-git && sbopkg -r"

alias emrsc="emacsclient -nw -a '' /etc/resolv.conf"

alias bigfan="dell-bios-fan-control 0 && i8kctl fan 2 2"

alias aria2c_lwf="aria2c -l "/tmp/RAMFS/$(date --iso-8601=seconds)-aria2-download.log" -x120 --min-split-size=148576 --split=120 --auto-file-renaming=false"

alias wechat="env LC_ALL=zh_CN.utf8 wine /home/lockywolf/.wine/drive_c/Program\ Files/Tencent/WeChat/WeChat.exe"
export LS_OPTIONS="$LS_OPTIONS --hide=*~ --hide=\#*\# "
alias ls="ls $LS_OPTIONS"
alias la="ls -lrht $LS_OPTIONS"
alias free="free -h"

alias winword="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msoffice2010/drive_c/Program\ Files/Microsoft\ Office/Office14/winword.exe"
alias powerpnt="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msoffice2010/drive_c/Program\ Files/Microsoft\ Office/Office14/powerpnt.exe"
alias msproject="WINEPREFIX=~/.wine_msoffice2010/ wine ~/.wine_msproject/drive_c/Program\ Files/Microsoft\ Office/Office14/winproj.exe"

