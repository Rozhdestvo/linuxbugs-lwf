#!/bin/bash

set -e -o pipefail

if [ "$#" -eq 0 ]; then
    printf "%s: No arguments supplied\n" "$0"
    exit 1
else
    filenames=( "$@" )
fi

WORKINGDIR="/tmp/bookmaker-$RANDOM"
printf "%s: WORKINGDIR=%s\n" "$0" "$WORKINGDIR"
rm -rf "$WORKINGDIR"
mkdir -p "$WORKINGDIR"

BASENAME="$WORKINGDIR/bookbuilder-latex-$RANDOM"
FILENAME="$BASENAME.tex"
printf "%s: FILENAME=%s\n" "$0" "$FILENAME"


#pdftk "$@" cat output $WORKINGDIR/pdftkoutput.pdf
cat > "$FILENAME" <<EOF
\documentclass{book}
\usepackage[margin=0.5in]{geometry}
\usepackage{pdfpages}
\usepackage{fancyhdr}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\lfoot{\textit{(Various papers)}}
\rfoot{(\thepage)}
\begin{document}
\tableofcontents
EOF

for i in "${filenames[@]}"
do
    TI="$(printf "%s" "$i" | sed "s/ /-/g"| tr -cd '[:alnum:]-').pdf"
    cp "$i" "$WORKINGDIR/$TI"
    printf "\chapter{%s}\n" "$TI" >> "$FILENAME"
    printf "\\includepdf[pages=1-,pagecommand={\\\thispagestyle{fancy}}]{%s}\n" "$TI" >> "$FILENAME"
done

cat >> "$FILENAME" <<EOF
\end{document}
EOF
#cat "$FILENAME"

#CWD=$(pwd)
pushd "$WORKINGDIR"
echo "$WORKINGDIR"
#cat "$FILENAME"
#exit 1
#ls
#sleep 2
lualatex "$FILENAME"
lualatex "$FILENAME"
popd
cp "$BASENAME.pdf" .
evince "$BASENAME.pdf"

#cp $WORKINGDIR/latexbuilder.pdf .

