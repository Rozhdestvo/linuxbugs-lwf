#!/bin/bash -x

set -euo pipefail
declare KERNEL_VERSION
KERNEL_VERSION="$(find /boot -iname 'vmlinuz-huge*' -type f | sort -r | sed 1q | cut -c 20-)"
printf "Last kernel: %s\n" "$KERNEL_VERSION"

/usr/sbin/sbopkg -B -i wireguard-linux-compat:KERNEL="$KERNEL_VERSION" \
		 -i nvidia-legacy390-kernel:KERNEL="$KERNEL_VERSION"  \
		 -i virtualbox-kernel:KERNEL="$KERNEL_VERSION" <<"EOF"
c
c
c
EOF

