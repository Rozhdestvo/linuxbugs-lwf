#!/bin/bash -x

declare MBSYNC_FILE
MBSYNC_FILE="/home/$(whoami)/.mbsyncrc"
DISPLAY=:0
export DISPLAY
declare ZENITY_ID
ZENITY_ID=f6835e48-177b-11eb-b604-5c260a7108b9
OWN_NAME="$0"


function check_mail ()
{
    if timeout 10m flock --verbose --nonblock --exclusive "$MBSYNC_FILE" mbsync -aV
    then
	printf "%s:checked mail successfully\n" "$OWN_NAME"

	pkill -f "$ZENITY_ID"
    else
	printf "%s:checking mail failed\n" "$OWN_NAME"
	zenity --notification --text="Failed to check mail with mbsync."
	if pgrep -f "$ZENITY_ID"
	then
	    # do nothing, the window is already there
	    true
	else
	    zenity --error --text="$(date --iso-8601=minutes):Failed to check mail" "$ZENITY_ID" &
	    disown
	fi
    fi
    emacsclient --eval "(mu4e-update-index)"
    NEW_MESSAGES=$(find ~/Mail-mbsync/ -mmin -7 -a -type f -not -iname '.uidvalidity' | wc -l)
    if (( NEW_MESSAGES != 0))
    then
	notify-send "Mbsync-messages:" \
		    "Received $NEW_MESSAGES messages"
    fi

}


time check_mail

#time mu index

