#!/bin/bash
set -e -o pipefail
addname=$(exiftool -Title -s -s -s "$1" | tr -d ' ')
echo "$addname"
#newname=$(tr -d ' ' "$newname")
#perl-rename -n "s/(.+).aac/\$1$newname.aac/" "$1"
#cp "$1"
oldbase=$(basename -s .aac "$1")
newname="renamed$oldbase$addname.aac"
cp "$1" "$newname"
