1)Kleopatra gibberish bug:
https://bugs.kde.org/show_bug.cgi?id=360364

2)Xfce4-screenshooter wrong command-line parameters:
https://bugzilla.xfce.org/show_bug.cgi?id=14513

3)scheme-complete compatibility with the Emacs 26.1
https://github.com/lockywolf/scheme-complete/commit/dea20b9212a3a849d6d2ed1d02c1a2c9bcaea901

4)Freedesktop registration bug
https://gitlab.freedesktop.org/freedesktop/freedesktop/issues/98

5)3)Wrong triangle orientation in GTK
https://bugzilla.xfce.org/show_bug.cgi?id=14614
WONTFIX

7)Lapacke clapack atlas-clapack

9)recaptcha on freedesktop https://gitlab.freedesktop.org/freedesktop/freedesktop/issues/98

8)typo on freedesktop https://gitlab.freedesktop.org/freedesktop/freedesktop/issues/99

=============================================

Not fixed:

1)Adding /opt/anaconda3/bin to the system path breaks the system's dbus.

2)Replacing packages with other packages in slackpkg.


4)Chinese fonts in Wine:
https://bugs.winehq.org/show_bug.cgi?id=45670

5)Xfce icons bugs
https://bugzilla.xfce.org/show_bug.cgi?id=14841

6)Xfce desktop icon startup delay

10)findutils -I{} -i
http://savannah.gnu.org/bugs/index.php?55190
