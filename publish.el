;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus
;; Time-stamp: <2020-11-02 19:44:11 lockywolf>

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.
;; Borrowed by Lockywolf at <2020-09-28 Mon 12:30>.
;; Used for inspiration:
;; 1. https://gitlab.com/ambrevar/ambrevar.gitlab.io/blob/master/publish.el
;; 2. https://ambrevar.xyz/blog-architecture/
;; Keywords: blog, publishing, org

;;; Code:

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'htmlize)
(package-install 'org-plus-contrib)
(package-install 'webfeeder)


(require 'org)
(require 'ox-publish)

(add-to-list 'load-path "hacks-replace-with-melpa-when-published")
(require 'ox-youtube)

(setq org-publish-use-timestamps-flag nil ;; bad idea in general
      org-publish-timestamp-directory "./")


;;; setting to nil, avoids "Author: x" at the bottom
;; on a Forge it is dubious
(setq user-full-name nil)


;;; ambrevar's functions.
(defun ambrevar/git-creation-date (file)
  "Return the first commit date of FILE.
Format is %Y-%m-%d."
  (with-temp-buffer
    (call-process "git" nil t nil "log" "--reverse" "--date=short" "--pretty=format:%cd" file)
    (goto-char (point-min))
    (buffer-substring-no-properties (line-beginning-position) (line-end-position))))

(defun ambrevar/git-last-update-date (file)
  "Return the last commit date of FILE.
Format is %Y-%m-%d."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))

(defun ambrevar/org-publish-find-date (file project)
  "Find the date of FILE in PROJECT.
Just like `org-publish-find-date' but do not fall back on file
system timestamp and return (org-time-string-to-time 0000-00-00) instead."
  (let ((file (org-publish--expand-file-name file project)))
    (or (org-publish-cache-get-file-property file :date nil t)
	(org-publish-cache-set-file-property file :date
	   (let ((date (org-publish-find-property file :date project)))
	     ;; DATE is a secondary string.  If it contains
	     ;; a time-stamp, convert it to internal format.
	     ;; Otherwise, use FILE modification time.
             (or
	      (let ((ts (and (consp date) (assq 'timestamp date))))
	       (and ts
		    (let ((value (org-element-interpret-data ts)))
		      (and (org-string-nw-p value)
			   (org-time-string-to-time value)))))
	      (org-time-string-to-time "0000-00-00")))))))
(advice-add 'org-publish-find-date :override 'ambrevar/org-publish-find-date)

(defun lockywolf/publish-sitemap-entry
    (file stype proj)
  "A custom version of `org-publish-sitemap-default-entry'.
FILE is filename. STYPE is usually {'tree,'list}. PROJ is project."
  (seq-concatenate 'string
		   (org-publish-sitemap-default-entry file stype proj)
		   " "
		   (let ((ambrevar-time (ambrevar/org-publish-find-date file proj)))
		     (if (not (equal (org-time-string-to-time "0000-00-00")
				     ambrevar-time))
			 (format-time-string "(%F)" ambrevar-time t)
		       "(unpublished)"))))

(defun lockywolf/extract-time-stamp-from-file-by-name (file)
  (with-temp-buffer
    (insert-file-contents-literally file)
    (if (string-match "Time-stamp: \<\\(.*\\)\>" (buffer-string))
	(match-string 1 (buffer-string))
      "")))

(defun lockywolf/org-html-format-spec (info)
  "Return format specification for preamble and postamble.
INFO is a plist used as a communication channel.
It is ambrevar's idea to overload it."
  (let ((timestamp-format (plist-get info :html-metadata-timestamp-format)))
    `((?t . ,(org-export-data (plist-get info :title) info)) ;; title
      (?s . ,(org-export-data (plist-get info :subtitle) info)) ;; subtitle
      (?d . ,(org-export-data (org-export-get-date info timestamp-format) ;; date
			      info))
      (?T . ,(format-time-string timestamp-format)) ;; export (generation) time
      (?a . ,(org-export-data (plist-get info :author) info)) ;; author
      (?e . ,(mapconcat ;; email
	      (lambda (e) (format "<a href=\"mailto:%s\">%s</a>" e e))
	      (split-string (plist-get info :email)  ",+ *")
	      ", "))
      (?c . ,(plist-get info :creator)) ;; creator
      (?C . ,(let ((file (plist-get info :input-file))) ;; file last modification time
	       (format-time-string timestamp-format
				   (and file (file-attribute-modification-time
					      (file-attributes file))))))
      (?v . ,(or (plist-get info :html-validation-link) "")) ;; validation-link
      (?q . ,(ambrevar/git-creation-date (plist-get info :input-file))) ;; git-creation
      (?w . ,(ambrevar/git-last-update-date (plist-get info :input-file))) ;; git-last-update
      (?r . ,(lockywolf/extract-time-stamp-from-file-by-name (plist-get info :input-file))))))

(advice-add 'org-html-format-spec :override 'lockywolf/org-html-format-spec)



;;; General export settings.

(setq org-export-with-section-numbers t
   org-export-with-smart-quotes t
   org-export-with-sub-superscripts '{} ; underlined_spaces
   org-export-with-toc t  ; top of the page toc hidden in css by me
                           ; borrowing code from ambrevar
   org-export-with-date t ; ?
   org-export-with-creator t)

;;; HTML export settings.

(defconst lockywolf/main-style-name "style.css")
(defconst lockywolf/org-style-name "org9-default-htmlize.css")
(defconst lockywolf/org-script-name "org9-default-htmlize.js")
(defconst atom-full.xml "atom-full.xml")
(defconst atom-titles.xml "atom-titles.xml")
(defconst feed-icon.svg "feed-icon.svg")
(defconst PayPal.svg "PayPal.svg")
(defconst Patreon.svg "Patreon.svg")


(setq org-html-use-infojs nil
 org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil ;; html is broken by design
      org-html-doctype "html5"

      org-html-head-include-default-style nil
      ;; I extracted it from `org-html-style-default' into "org9-default-htmlize.css".
      ;; It will stabilise the style between updates, but may be prone to
      ;; vulnerabilities.
      org-html-head-include-scripts nil
      ;; I extracted it from `org-html-scripts' into "org9-default-htmlize.js".
      ;; It will stabilise the style between updates, but may be prone to
      ;; vulnerabilities.
      
      org-html-head-extra
      (seq-concatenate 'string
		       "<link rel=\"stylesheet\" href=\"/"
		       lockywolf/main-style-name
		       "\">
<link rel=\"icon\" href = \"favicon.ico\"  type = \"image/x-icon\">
<link rel=\"feed\" href=\"/" atom-full.xml "\" title=\"Articles (Full)\">
<link rel=\"feed\" href=\"/" atom-titles.xml "\" title=\"Articles (Titles)\">
<link rel=\"stylesheet\" href=\"/"
		       lockywolf/org-style-name
		       "\">
<script src=\"/"
		       lockywolf/org-script-name
		       "\" type=\"text/javascript\" ></script>")

      
      
      org-html-preamble t
      org-html-preamble-format (list ( list "en"
				  (seq-concatenate 'string
   "<nav class=\"navbar\">
    <a href=\"https://lockywolf.net/\">Home</a>
    <a href=\"http://blog.lockywolf.net/\">Blog</a>
    <a href=\"/2020-10-29_scheme-story.html\">Scheme</a>
    <a href=\"/sitemap-notes.html\">Notes</a>
    <a href=\"/sitemap-doc.html\">Howtos</a>
    <a href=\"/" atom-full.xml "\"><img src=\"/" feed-icon.svg "\" />Full</a>
    <a href=\"/" atom-titles.xml "\"><img src=\"/" feed-icon.svg "\" />Titles</a>
    <a href=\"/lslr.txt\">ls-lR</a>
</nav>
<div id=\"donate-block\">
<p>If you like what I am doing, do not hesitate to donate, it helps me do more fun and useful stuff.
</p>
<p>
  <a href=\"https://paypal.me/independentresearch\"
     style=\"border:solid; 
       border-color:#000077; 
       text-decoration: none;\">
  <img id=\"paypal-donation-button\"
      alt=\"Donate with Paypal\"
      src=\"/" PayPal.svg "\"
    style=\"height: 0.75em; margin-left: 0.5ex;\" />
Donate!
  </a>
  <a href=\"https://www.patreon.com/bePatron?u=29080977\"
     style=\"border:solid; 
       border-color:#700000;
       text-decoration: none;\"
     data-patreon-widget-type=\"become-patron-button\">
  <img id=\"patreon-donation-button\"
      alt=\"Become a Patreon Patron\"
      src=\"/" Patreon.svg "\"
      style=\"height: 0.75em; 
              margin-left: 0.5ex;
              background-color: red;\" />
Become a Patron!
  </a>
  <!-- <script async src=\"https://c6.patreon.com/becomePatronButton.bundle.js\">
  </script> -->
  <!-- <noscript> -->
  <a href=\"https://liberapay.com/independentresearch/donate\"
     style=\"border: solid; 
             border-color: #707000;
             text-decoration: none;
             padding-right: 0.5ex; \">
  <img id=\"liberapay-donation-button\"
      alt=\"Donate using Liberapay\"
      src=\"https://liberapay.com/assets/widgets/donate.svg\"
      style=\"height: 0.75em; 
              margin-left: 0.5ex;\"
       />
Donate!
    </a>
  <!-- <script async src=\"https://liberapay\"> -->
  <!-- </noscript> -->
</p>
</div>
<style>
\#donate-block {
text-align: center;
}
\#donate-block p{
margin:0.5ex;
}
</style>") )) ;
      org-html-postamble t
      org-html-postamble-format
      (list (list "en"
		  "
<div class=\"comments\">
<h2> Comments </h2>
<p>
Comments welcome. Submit your comment by sending an email to:<br/>
<a href=\"mailto:lockywolf.net.comments@《remove》lockywolf.《remove》net?subject=%t\">
mailto:lockywolf.net.comments@《remove》lockywolf.《remove》net?subject=\"%t\"</a>
</p>
</div>
<div class=\"colophon\">
<p class=\"author\">Author: %a</p>
<p class=\"date\">Published: %d</p>
<p class=\"date\">Last edited: <span id=\"last-edited-time-stamp\">%r</span></p>
<p class=\"date\">Last file modified: %C</p>
<p class=\"date\">Page generated: %T</p>
<p class=\"date\">Added to git: %q</p>
<p class=\"date\">Git modified: <span id=\"git-modified-time-stamp\">%w</span></p>
<p class=\"creator\">%c</p>
<p class=\"license\">
  <a rel=\"license\"
     href=\"http://creativecommons.org/publicdomain/zero/1.0/\">
        <img alt=\"Creative Commons License\"
             style=\"border-width:0\"
             src=\"https://licensebuttons.net/p/zero/1.0/80x15.png\" />
  </a>
</p>
</div>"))
      )




(setq org-html-infojs-options
   '((path . "https://orgmode.org/org-info.js")
     (view . "showall")
     (toc . :with-toc)
     (ftoc . "0")
     (tdepth . "max")
     (sdepth . "max")
     (mouse . "underline")
     (buttons . "0")
     (ltoc . "0") ; tiny TOCs at the end of each page
     (up . :html-link-up)
     (home . :html-link-home)))

(defvar site-attachments
  (regexp-opt '("ico" "cur" "css" "js" "woff" "html" "pdf" "txt"))
  "File types that are published as static files.")

;; (defun lwf-gitlab-publish-attachment (PLIST FILENAME PUB-DIR)
;;   "Work around gitlab symlinks."
;;   (if (file-exists-p FILENAME)
;;       (org-publish-attachment PLIST FILENAME PUB-DIR)
;;     (org-publish-attachment PLIST "./placeholder" PUB-DIR)))


(setq org-publish-project-alist
      (list
       (list "site-notes-org"
             :base-directory "./notes"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
	     :table-of-contents nil
	     :auto-sitemap t
             :sitemap-filename "sitemap-notes.org"
	     :sitemap-title "Various Notes"
	     :sitemap-format-entry #'lockywolf/publish-sitemap-entry
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically
	     :with-broken-links t
	     :makeindex t)
       (list "site-notes-static"
             :base-directory "./notes"
             :exclude "draft"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
	     :with-broken-links t
             :recursive t)
       (list "site-doc-org"
             :base-directory "./doc"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
	     :table-of-contents nil
	     :auto-sitemap t
             :sitemap-filename "sitemap-doc.org"
	     :sitemap-title "HOWTOs"
	     :sitemap-format-entry #'lockywolf/publish-sitemap-entry
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically
	     :with-broken-links t)
       (list "site-doc-static"
             :base-directory "./doc"
             :exclude "draft"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
	     :with-broken-links t
             :recursive t)
       (list "site-manual-org"
             :base-directory "./website-prototype"
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft" "readme.org"))
	     :table-of-contents nil
	     :auto-sitemap nil
	     :with-broken-links t)
       (list "site-manual-static"
             :base-directory "./website-prototype"
             :base-extension (list)
	     :exclude ".*"
	     :include (list "favicon.ico"
			    lockywolf/org-style-name
			    lockywolf/org-script-name
			    lockywolf/main-style-name
			    feed-icon.svg
			    PayPal.svg
			    Patreon.svg)
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
	     :with-broken-links nil
             :recursive nil)
       (list "site" :components
	     '("site-manual-org"
	       "site-manual-static"
	       "site-notes-org"
	       "site-notes-static"
	       "site-doc-org"
	       "site-doc-static"))
       ))

(org-publish-project "site")

(defun lockywolf/webfeeder--format-atom-author (author)
  "TODO: this function is a crude patch over the broken function in webfeeder.
AUTHOR is sometimes not what it expects."
  (concat "<author>"
          (let ((name+addr (webfeeder--extract-name+email author)))
            (if (cadr name+addr)
                (format "<name>%s</name><email>%s</email>"
                        (xml-escape-string (cadr name+addr))
			"hidden"
                        ;; (cadr name+addr)
			)
              (format "<name>%s</name>" (xml-escape-string author))))
          "</author>\n"))

(advice-add 'webfeeder--format-atom-author
	    :override
	    'lockywolf/webfeeder--format-atom-author)


(defun my-date-libxml (html-file)
  "Return the date from the HTML-FILE.
The date is returned as time value.  See `current-time-string'.
This requires Emacs to be linked against libxml."
  (with-temp-buffer
    (insert-file-contents html-file)
    (let* ((dom (libxml-parse-html-region (point-min) (point-max)))
           (date-last-edited (dom-text (car (dom-by-id dom "last-edited-time-stamp"))))
	   (date-git-edited (dom-text (car (dom-by-id dom "git-modified-time-stamp")))))
      (cl-labels ((date-extract (date-last-edited)
				(and date-last-edited
				     (string-match (rx (group (repeat 4 digit) "-" (repeat 2 digit) "-" (repeat 2 digit)))
						   date-last-edited)
				     (match-string 1 date-last-edited))))
       (date-to-time
	(concat (or (date-extract date-last-edited)
		    (date-extract date-git-edited)
		    (date-extract "0001-01-01"))
		" DummyDateSuffix"))))))

(defun my-author-query-fake (file-name)
  "FILE-NAME ignored."
  "lockywolf")

(defun my-filter-out-generated (webfeed-entry)
  "WEBFEED-ENTRY is a file to be tested for publish-ability."
  (if (string-match "^.*/sitemap-.*html$" (webfeeder-item-url webfeed-entry))
      nil
    t))

;; Ignore logo, too lazy.
;; https://snook.ca/archives/rss/add_logo_to_feed/
;; (defun my-link-hack (old-function html-file)
;;   "OLD-FUNCTION HTML-FILE."
;;   (seq-concatenate 'string
;; 		   (apply old-function (list html-file))
;; 		   "
;; <icon>http://example.org/favicon.ico</icon>"))



(let ((webfeeder-date-function 'my-date-libxml)
      (webfeeder-author-function 'my-author-query-fake)
      (list-of-files (mapcar (lambda (f) (replace-regexp-in-string "^public/" "" f))
			     (directory-files-recursively "public" ".*\\.html$"))))
  (webfeeder-build
   atom-full.xml
   "./public"
   "https://lockywolf.net/"
   ;; (mapcar (lambda (f) (replace-regexp-in-string ".*/public/" "" f))
   ;;         (directory-files-recursively "public" "index.html"))
   list-of-files
   :predicate #'my-filter-out-generated
   :title "Vladimir Nikishkin: Homepage"
   :description "(Abstract Abstract)")
  (cl-labels ((webfeeder-body-function ( whatever . o )
				       "Visit the website."))
    (let ((webfeeder-body-function #'webfeeder-body-function))
      (webfeeder-build
       atom-titles.xml
       "./public"
       "https://lockywolf.net/"
       ;; (mapcar (lambda (f) (replace-regexp-in-string ".*/public/" "" f))
       ;;         (directory-files-recursively "public" "index.html"))
       list-of-files
       :predicate #'my-filter-out-generated
       :title "Vladimir Nikishkin: Homepage"
       :description "(Abstract Abstract)"))))


(provide 'publish)
;;; publish.el ends here
