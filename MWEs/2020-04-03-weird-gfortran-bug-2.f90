
program main

  type, abstract :: scheme_object
  end type scheme_object
  type, extends(scheme_object) :: scheme_primitive_procedure
     procedure(packageable_procedure), pointer, nopass :: proc_pointer
  end type scheme_primitive_procedure
  
  class(scheme_object), pointer :: exp

  abstract interface
     function packageable_procedure( argl, env ) result( retval )
       import :: scheme_object
       class(scheme_object), pointer :: retval
       class(scheme_object), pointer :: argl
       class(scheme_object), pointer :: env
     end function packageable_procedure

  end interface
  
  
contains
  function low_level_read( arg ) result( retval )
    class(scheme_object), pointer :: retval
    class(scheme_object), pointer :: arg
    
  end function low_level_read

  subroutine main_loop()
    procedure(packageable_procedure), pointer :: proc
    class(scheme_object), pointer :: retval
    exp => low_level_read( exp )
    call ll_setup_global_environment()
    select type( proc_holder => low_level_read(exp) )
    type is (scheme_primitive_procedure)
       proc => proc_holder%proc_pointer
       retval => proc( exp, exp )
    class default
       error stop "primitive procedure not a procedure"
    end select
    
  end subroutine main_loop
end program
