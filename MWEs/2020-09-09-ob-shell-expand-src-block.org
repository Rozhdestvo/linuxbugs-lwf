# -*- mode: org; -*-

* test
  :PROPERTIES:
  :header-args:    :noweb yes
  :END:

#+name: block1
#+begin_src shell
printf "Block1 \n"
#+end_src


#+name: block2
#+begin_src shell
printf "Block2 \n"
#+end_src


#+begin_src shell :shebang "#!/bin/chibi-scheme"
  <<block1>>
  <<block2>>
#+end_src

#+RESULTS:

