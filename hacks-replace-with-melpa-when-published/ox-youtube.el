;;; ox-youtube.el --- YouTube embedded video export for org-mode.  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Vladimir Nikishkin

;; Author: Vladimir Nikishkin <lockywolf@gmail.com>
;; Keywords: convenience, multimedia, outlines

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides org-export-html for youtube links embedding.
;; odt, etc, not supported, pull requests/patches welcome.
;; LaTeX supported in an improvised way.

;; Original idea:
;; https://endlessparentheses.com/embedding-youtube-videos-with-org-mode-links.html

;;; Manual

;; You need to insert links as in [[yt:sTr4ng3d0d3]], as this is the
;; original design. Support for links like
;; [[youtube:https://www.youtube.com/watch?v=sTr4ng3d0d3]] is not implemented.

;;; Patches wanted
;; Patches wanted for: vimeo, bilibili, youku, qiyi, rutube, all other platforms.
;; Width support as in: yt:sTr4ng3d0d3:width:50ex:height:30ex, or smth like.

;;; Code:

;; (defvar yt-iframe-format
;;   ;; You may want to change your width and height.
;;   (concat "<iframe width=\"440\""
;;           "       height=\"335\""
;;           "          src=\"https://www.youtube.com/embed/%s\""
;;           "  frameborder=\"0\""
;;           " allowfullscreen>
;;             %s
;;             </iframe>"))

(defvar yt-iframe-format
  (seq-concatenate 'string
 "<style>.embed-container {
 position: relative; 
 padding-bottom: 56.25%%; 
 height: 0; 
 overflow: hidden; 
 max-width: 100%%; 
 }
 .embed-container iframe, .embed-container object, .embed-container embed {
 position: absolute; 
 top: 0;
 left: 0;
 width: 100%%;
 height: 100%%;
 }
 </style>
 <div class='embed-container'>
   <iframe src='https://www.youtube.com/embed/%s' frameborder='0' allowfullscreen>
   </iframe>
 </div>")
 )  ; "Taken from https://embedresponsively.com/."

(org-add-link-type
 "yt"
 (lambda (handle)
   (browse-url
    (concat "https://www.youtube.com/embed/"
            handle)))
 (lambda (path desc backend)
   (cl-case backend
     (html (format yt-iframe-format
                   path (or desc "")))
     (latex (format "\href{%s}{%s}"
                    path (or desc "video"))))))

(provide 'ox-youtube)
;;; ox-youtube.el ends here
