# Time-stamp: <2020-10-27 16:58:53 lockywolf>
#+created: <2020-07-21 Tue 11:19>
#+date: <2020-07-21 Tue 11:19>
#+author: lockywolf gmail.com
#+title: How to Read Books.

* Abstract

This document records some practices I found myself using while reading texts.
It is not at all exhaustive, complete or even sufficiently encompassing.
It does not claim to be efficient.
It is a work in progress.
It is open for discussion.
I intend to write down here some practices that I find useful, sometimes providing justification.


* Reading setup

Reading is a thing that can be computer-assisted nowadays, and if it can be, it should be.
I try to do the following:

** Print out the book

Unless the book is about illustration or graphics, black-and-white is enough.
I print it a typographic shop, ask for paperback binding and manually write the name at the book spine.

** Have an electronic version open before you

I usually read PDF books, about 90%.
I have the PDF file open either in Evince, or in Emacs pdf-tools.
This simplifies searching.
In 10% of the cases this is either an HTML or an EPUB version, which I still usually convert to PDF, or try to open in Evince.

** Have Google prepared

I end up googling quite a lot, so I have Google open in the browser (Firefox) for quick access.

** Have a dictionary open

I usually use Google Translate, and it's enough just about 90% of the time.
In the 10%, I use Wordreference, BKRS and mdbg.

** Pencil and eraser

I use a mechanical pencil (I do not like sharpening pencils), 0.9HB.
I have an eraser too, relatively hard.
I do not use coloured pens or pencils for annotation.

** Ruler

I use a ruler to focus on reading, and to sometimes demarcate important pieces of the text.

** Smartphone with a Chinese character input method

I use it when reading Chinese material.
Finding individual characters for input tends to be easier using handwritten recognition, than by radical search.
Using CangJie may be easier, but I do not know it yet.

* Reading practice

** Timing

I use Emacs org-mode's time tracking capabilities for measuring how much time exactly a book takes.
It also helps me stay focused on reading, since I do not want to obscure time tracking data.

** Notes file

I try to have note files for books I read.
Emacs, and other special software has special functions for making notes, but I did not find a way to use them efficiently.
This only relates to humanities, light, or fiction literature. 
Scientific literature I process differently.

The notes file is an org-file.
It consists of two root nodes: a node for vocabulary and a node for remarks.

*** Vocabulary node

The vocabulary node is just called "vocabulary".
It contains just a single table of the following format:

| # | unknown word or phrase | translation |
|---+------------------------+-------------|
| 0 | sepulka                | сепулька    |

I usually do not fill in the dictionary at the moment of reading.
I consider this a separate task, to be done later.
(Maybe this should not be done like this?)

*** Citations and Remarks node

In the Citations and Remarks node, every heading corresponds to a piece of the text that made me generate a non-trivial thought.
The thought is written in the body of the heading.

This is where have both the digital and the paper copy comes in handy.
When I find an interesting piece of text, I can search it in the electronic copy and copy-paste into the notes file.

** Reading using a ruler

I heard that it is recommended to use a "sliding window" to read text.
I do not use it. 
However, I use a ruler to protect my eyes from wandering ahead of the narrative.
I put it right under the line I am reading an move down after the line has been read.

** Paper annotation

As mentioned above, I do not use colours for annotations, because I don't know how to make them efficient.
(Suggestions welcome.)

Apart from colours, there are the following markup tools:

- underline the text
- circle the text
- put an exclamation mark at the margins
- squeeze a remark between the lines

I tend to underline words and phrases that attracted my attention.
If they seem noteworthy, I then copy them into the notes file remarks section.
I tend to circle the words that are unfamiliar or unknown.
I try to copy them to the notes file vocabulary section.
I sometimes use a ruler to mark some extremely important pieces of text.

I sometimes circle "large sparse" pieces of text which make little sense and cross them out with several strokes to obscure.

Sometimes I write remarks on the margins.
Sometimes I squeeze remarks in between the lines.
Both of the above are not very efficient.


** Googling

I tend to google work meanings if I don't understand them on the spot, but do not write the meanings into the table.
This is because I want to visit the vocabulary again, and have some "context to remember".

I tend to google concepts that I do not know, but I do not write them anywhere.
(Shall I have the third section in the notes file?)


* Reading tricks

** Read the table of contents first

** Read the bibliography before reading the text

** Read the introduction

** Read the first paragraph of very chapter

** Read the full story from the start to the end

** Re-read the most interesting pieces

** Write a short review for yourself

Do not try to make it a full-scale university-level essay.
It would be a waste of time.
But try to reiterate all the thoughts that you found useful, well versed, or non-trivial.

** Discuss the material with friends or random people


