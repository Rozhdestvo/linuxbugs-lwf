# Time-stamp: <2020-10-27 19:07:38 lockywolf>
#+date: <2020-07-30 Thu 20:44>
#+author: lockywolf gmail.com
#+title: Notes on understanding Android.

* Storage 

Android storage is really mind-boggling.

Background reading: https://www.kernel.org/doc/Documentation/filesystems/sharedsubtree.txt

https://android.stackexchange.com/questions/203951/how-can-i-make-a-symlink-or-equivalent-inside-storage-emulated-0/

sdcardfs : https://www.xda-developers.com/diving-into-sdcardfs-how-googles-fuse-replacement-will-reduce-io-overhead/

* Security

https://android.stackexchange.com/questions/208523/how-androids-permissions-mapping-with-uids-gids-works/208982#208982

https://android.stackexchange.com/questions/210139/what-is-the-u-everybody-uid/210159#210159

Selinux?

https://android.stackexchange.com/questions/210139/what-is-the-u-everybody-uid/

Runtime permissions

https://source.android.com/devices/storage#runtime_permissions

Namespaces 

https://android.stackexchange.com/questions/197959/why-partition-gets-unmounted-automatically-after-some-time/200449#200449

https://android.stackexchange.com/questions/214288/how-to-stop-apps-writing-to-android-folder-on-the-sd-card/

https://android.stackexchange.com/questions/217741/how-to-bind-mount-a-folder-inside-sdcard-with-correct-permissions/217936#217936

https://developer.android.com/training/data-storage#scoped-storage


