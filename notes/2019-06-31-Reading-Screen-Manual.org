# -*- mode:org; eval: (visual-line-mode) -*-
# Time-stamp: <2020-10-27 19:02:23 lockywolf>
#+title: Reading manual for GNU Screen.
#+created: 2019-06-31

* Questions
** What's the difference between xterm and uxterm?
Answer: TODO
** What is the difference between DEC VT 100 and other DEC terminals?
Answer: TODO
** What is the ISO-10646-1 standard?
Answer: TODO
** What is the ISO 6429 (ECMA 48, ANSI X3.64) format?
Answer: TODO
** What is the ISO 2022 standard?
Answer: TODO
** Why don't I have /etc/utmp?
Answer: TODO. I do have a /var/run/utmp , although no /run/utmp .
** What do the commands talk, script, shutdown, rsend, sccs do?
Answer: TODO.
** What do the commands tset, qterm do?
*** How does term=foobar work?
Answer: TODO
** What is .logout file?
Answer: TODO
** How to efficiently edit tree-like data in Emacs?
Answer: org-mode?
** What are all the nice things I needed to google after reading the Emacs Manual?
Answer: TODO.
** What is the difference between w, who and finger?
w and who definitely parse utmp (which is probably /var/run/utmp on
Slackware Linux). But how does Finger find out who's logged in? 

** What is actually RSA?
*** How does encryption with RSA work?
*** How does signing with RSA work?

** What is DSA?
** Can it do both encryption and signing?

** What is ECDSA?

** What is ED25529?

** What is SCTP?

** What is the use of ssh -f? As well as autossh -f? How is it different from using &?

** Why does rscreen by default rebinds C-a to C-z?

** What's pfexec?

** Replace fail2ban with blacklistd

** What is /etc/hosts.allow?
