# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2020-10-27 18:55:23 lockywolf>
#+created: 2019-06-19T15:09:44
#+author: lockywolf gmail.com
#+title: Reading "SSH Mastery" by Michael Lucas.

This file is written while I was reading "SSH Mastery" by Michael W. Lucas.

* Questions
** What is ECDSA? Is is an improved version of DSA? Why not just RSA?
** What is ED25519? Is it any better than RSA?
** How can I check if my computer was visited by "Hail Mary Cloud"?
** How does a passphrase actually protect the ssh key? Other encryption layer?
** What does xauth(1) do?
** Let me have an X-server running on localhost:10.0 ; what do 10 and 0 mean？
** xscreensaver or xlockmore? How does xfce4-screensaver choose one?
** What is DSA? How does it work?
** What is sha2?
** What is nistp256?
** How does X.509 system work?

* Words
** aggravating
** decoy
** piggyback
** tinpot despot
