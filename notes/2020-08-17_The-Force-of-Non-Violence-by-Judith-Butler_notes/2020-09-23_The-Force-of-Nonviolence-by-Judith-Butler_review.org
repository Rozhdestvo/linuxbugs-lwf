#+BLOG: wordpress
#+POSTID: 291
# -*- mode: org; eval: (visual-line-mode) -*-
# Time-stamp: <2020-09-23 11:14:25 lockywolf>
#+date: <2020-09-23 Wed 10:23>
#+author: lockywolf gmail.com
#+title: Musings on "The Force of Non-Violence" by Judith Butler (2020).
#+category: review, 
#+tags: book-review, communication, english, literature, philosophy, politics, psychology, review, 

* The Force of Non-Violence by Judith Butler (2020)

[[file:0001_cover_The-force-of-Non-violence-e1583843807795.png]]

A long, confusing, self-inconsistent and contradictory "review" is following.

@@html:<!--more-->@@

** The book.

[[file:0002_confusion_content-confusion.jpg]]

This book is very messy.
It uses a very obscure language, and it is missing supporting links for several critical statements.
(It does provide references to many other statements.)
Even the amount of words I had to write down into the "learn later" list was way smaller than similar texts give me, as the confusion was mainly coming from the misuse of easier words, rather than from using many complicated ones.

As the book is badly written, and extracting the meaning from the text is very hard, this review would have to be in the form of "what thoughts the book made think", rather than "what is the book about".

So, the whole book is based on the concept of "imaginary".
This book is more about a Utopian fantasy than about anything existing.
Butler herself is supporting her right to do so with a weakest supporting argument of all times "would you like to live in a world in which no-one thinks about such a development perspective".
Indeed, this can be rephrased as "you what, care too much about what I write?".
It is basically an appellation to Freedom of Speech, that is a last resort.

I do grant her that right, but then I would rather have to be using this text not as something describing anything working, but rather as an exercise in reading and extracting whatever meaningful thoughts there may be in a deliberately obscured text.
An exercise in philosophising rather than an exercise in philosophy. 

** Useful thoughts.

[[file:0003_useful-thoughts_freud.jpg]]

The author seems to be determined that the human world in general can be described using essentially two main tools: politics and psychology.
Not digging very deeply, she seems to be using Freud and his theory as a source of psychological theory, and Hobbes as the source of political theory.
While both are very respectable founders of the fields, it staggered me she seems to be regarding them so highly even being aware (?) of the modern state of research.
The idea here, perhaps, is that politics describes human behaviour "en masse", whereas psychology describes interpersonal (she uses the word "dyadic", which is already strange enough) interactions.
But, it's obvious, but someone has to say that: human societies are so much more complex than just one-to-one and one-to-many interactions!

Butler seems to love Freud.
It is a little surprising, given that Freud is not, probably, the most advanced source of psychological knowledge nowadays.
Indeed, he was extremely instrumental in founding the field, but that was so long ago, and much more substantive research has been done since.

She also seems to be focused mostly on Hobbes when looking at political theory.
She acknowledges the existence of Locke and Rousseau, but very superficially, and mostly with respect to the "state of nature".
(Rousseau is mentioned twice less than Hobbes, and Locke's theory is even completely ignored. Furthermore, why do we even need the "state of nature" in this discussion?)
And again, why are we focusing on the founders so much more than on the ones who attempted to improve the theory?

She tries to "imagine" the world that is non-violent (which is a very confusing an convoluted term that she spends a lot of time describing).
This world, she argues, has to be based on the "ethics of interdependence".
The value of life, she argues, is based on the "grievability" by other people, then.

The argument she's trying to build, jumps from the imaginary "freedom as independence" into the "freedom of total inter-dependence".
It jumps as if there is no middle ground.

She does not completely ignore the existence of groups, but entirely ignores the very concept of group dependence.
Indeed, people cannot be fully independent, but total dependence is also not how things work.
Freedom (the word she doesn't use a lot) means that people _choose_ whom upon to depend.

The groups that appear in her text are mostly the groups of similarity, and most often the groups of blood relatives.
She also speaks about the groups of grievability and groups of power, but almost entirely ignores the groups of friendship and cooperation.
Whereas those constitute those groups that are actually worth living for.

** Living

[[file:0004_life_what-is-life.jpg]]

The concept of "living" plays a large role in her argument, and she writes a lot of words to try and describe what it is to be alive, and how we regret the loss of life.
She even proposes to value lives according to how much we would regret the loss of those lives...

But why would we even do that?
That's very human and emotional to grieve _after_ the loss.
But that very notion has been long proven to be one of the least useful in the world.
As an English proverb says "There's no sense crying over spilt milk."

The value of the life that is already lost is then known precisely, and equal to zero.

The confusion is exacerbated by the fact that she dodges the urge to at least define what it is to be living.
I know that some mental gymnastics needs to be done in order to declare the "green" position life-preserving and the "pro-life" position non life-preserving, but hey, it doesn't have to be that inconsistent.
(It is also discomfiting how much American politics is repeatedly influencing the world outside the U.S. in a way largely irrelevant to the outside world.)

** Imagination

[[file:0005_imagination_o-MIND-IMAGINATION-facebook.jpg]]

The concept of imagination is used a lot in her treatise.
This is indeed a place where this book has proven to be useful.
Indeed the result of her imagination I consider to be worthless, but the way she self-reflects on imagining the world, and also tries to model the way other people imagine the world, made me think a lot.

I have never really thought about the "imagination machinery" in the human brain.
And I really like the concept of phantasy, distinct from fantasy by the presence of subconscious component.
I really liked to think about different kinds of imagination as in: imagining scenes, imagining words, both written and spoken, imagining 2d objects, imagining feelings, and much more.

I think that the "imagination software" in the brain is really worth exploring.

** Saving and killing.

[[file:0006_rescue_inx960x640.jpg]]

The part of the treatise that is dedicated to saving and killing largely rotates about the desire to destroy and the desire to save as given by Freud.
I cannot say that I can distil any meaningful conclusion from her words.
Moreover, all the discussion seems very contrived and produced only in the name of deriving certain political slogans of the day.
That is, it looks (to me) as largely just fitting the argument to the answer that the author already believes to be true.
The very structure that is dedicated to preserving life, she considers to be a manifestation of a "dominance hierarchy" that is worth bringing down.
Indeed, often such structures become corrupt, but she produced no decent substituting concept, besides doing some mental gymnastics modelled after Kant and Freud.

She does give a great account on the police in the USA killing people, especially black people.
The language of those parts of the book is much more lucid and provides a much more vivid image.
This makes me think that as a political professional (e.g. a political campaign mastermind) she could have had a role that would fit her much better than the one of a professional philosopher.

** Some thoughts on basic instincts.

[[file:0007_instincts_the_three_laws_of_robotics.png]]

This section will just list a few thoughts that I don't think actually fit into any reasonable piece of argument, but are worth scavenging from the book.

- Empathy is done through reflection. We never really empathise with anyone, but only with ourselves imagined in the position of the one being empathised with.
- Treating others well, we compensate for ourselves not being loved enough. (Really?)
- "Make die" and "let die" both cause someone's death, but the second one "let die" is generally much less morally discouraged.
- "Motivations" are actually neither conscious nor unconscious, but in between.

** Some thoughts on the writers she cites.

[[file:0008_bibliography_books.jpg]]

Obviously, as mentioned previously, she mentions Freud and Hobbes.
As one of the Freud's successors, she speaks about Anna Klein.
She cites almost all famous Marxists of the 21st century, starting from Althusser.
Laplanche and Derrida, obviously, creep into the narrative too, how could they have not.
Foucault and Fanon even get their own chapter.

Melanie Klein seems to be the only psychologist that she seriously considers, besides Freud.
Perhaps, worth looking into.

Unmentioned in the book, but seems related to me is the work of the late Sir Roger Scruton -- Art and Imagination: A Study in the Philosophy of Mind. 

** The actual force of the non-violent action.

[[file:0009_protests_Moscow_rally_at_the_Bolotnaya_square_10_Dec_2011_1.jpg]]

The most disappointing part of the book is that the actual analysis of the non-violent action is given as little attention as possibly can be given so long that the books still bears some relevance to the non-violent protest.
Apart from "using human bodies as a wall" and "coming to the shores of Europe in boats full of people", not much is spoken about ethicality (or the absence thereof) of different kinds of peaceful protest.
Strategy, tactics, effectiveness -- all of that is mostly ignored, apart from insisting that the structural violence itself would always try to present peaceful action as violent.
As if we wouldn't know that.
A lot is said about self-defence, but very little about extralegal defence of the others. 
In particular, the subject of defending humans against dangerous forces of nature is completely ignored.

** Conclusion

[[file:0010_perplexity_ch01.gif]]

When you are approaching someone, who has been well-known as an adversary of the forces you generally sympathise with, there may be several expectations.
You may expect the text to be an outrageous demagoguery, aimed at appealing to emotions and ignoring any traces of rational.
This at least gives you the feeling of guilty pleasure by imagining a picture of a wild combat of ideas.
You may expect a cleverly twisted argument, that is crafted so well that you find it very hard to penetrate the logic and to find flaws.
Then it is upon you to sharpen your mind in order to have a proper duel with your opponent.
You can expect to be mistaken, and be exposed to the ideas that have not yet had a place in your mind, and that would be the best possible option.
Getting enlightened, after all, is one of the best feelings in the world.

What you probably do not expect, although you should, is the book to be just lacking any sort of cohesive picture in it.
Is it of the aspects of the "banality of evil" that Hannah Arendt was writing about?
"The Force of Non-Violence" is from the last category.
Of course, I am not equating Butler with any of the horrible evil-doers of the world.

But the book still leaves me with that creeping in feeling: 
"How can it be that someone who manages to take simple things and express them in a totally incomprehensible way happens to be one of the most prominent philosophers of our time?"

It leaves you with a feeling that you have missed something.
Is that "something" ultimately incomprehensible to just that kind of people that you belong to?

But no, over and over I keep seeing in this book only an exercise in philosophising and nothing else.
Chunks of not very consistent reasoning interspersed with literature reviews of various philosophers and journalists.
Attempts to make a well-structured text that keep failing over and over.

On the other hand, at least it has made me produce the longest so far book review I have made.
At least that I should be grateful for.

* Contacts

Subscribe and donate if you find anything in this blog and/or other pages useful.
Repost, share and discuss, feedback helps me become better.

I also have:
- Facebook :: http://facebook.com/vladimir.nikishkin
- Telegram :: http://t.me/unobvious
- GitLab ::   http://gitlab.com/lockywolf
- Twitter :: https://twitter.com/VANikishkin
- PayPal :: https://paypal.me/independentresearch


# 0001_cover_The-force-of-Non-violence-e1583843807795.png http://lockywolf.files.wordpress.com/2020/09/0001_cover_the-force-of-non-violence-e1583843807795.png
# 0002_confusion_content-confusion.jpg http://lockywolf.files.wordpress.com/2020/09/0002_confusion_content-confusion.jpg
# 0003_useful-thoughts_freud.jpg http://lockywolf.files.wordpress.com/2020/09/0003_useful-thoughts_freud.jpg

# 0004_life_what-is-life.jpg http://lockywolf.files.wordpress.com/2020/09/0004_life_what-is-life.jpg

# 0005_imagination_o-MIND-IMAGINATION-facebook.jpg http://lockywolf.files.wordpress.com/2020/09/0005_imagination_o-mind-imagination-facebook-1.jpg
# 0006_rescue_inx960x640.jpg http://lockywolf.files.wordpress.com/2020/09/0006_rescue_inx960x640.jpg
# 0007_instincts_the_three_laws_of_robotics.png http://lockywolf.files.wordpress.com/2020/09/0007_instincts_the_three_laws_of_robotics.png
# 0008_bibliography_books.jpg http://lockywolf.files.wordpress.com/2020/09/0008_bibliography_books.jpg
# 0009_protests_Moscow_rally_at_the_Bolotnaya_square_10_Dec_2011_1.jpg http://lockywolf.files.wordpress.com/2020/09/0009_protests_moscow_rally_at_the_bolotnaya_square_10_dec_2011_1-2.jpg
# 0010_perplexity_ch01.gif http://lockywolf.files.wordpress.com/2020/09/0010_perplexity_ch01.gif
